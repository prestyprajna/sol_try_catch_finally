﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Try_Catch_Finally
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new CalculationPrint().PrintMethod();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }
        }
    }

    public class Calculation
    {
        public void DivisionCalculation()
        {
            try
            {
                int val = 8;
                int result = val / 0;
            }
            catch (Exception)
            {

                throw;
            }            
        }       
    }

    public class CalculationPrint
    {
        private Calculation _calObj = null;

        public CalculationPrint()
        {
            _calObj = new Calculation();
        }

        public void PrintMethod()
        {
            try
            {
                _calObj.DivisionCalculation();
            }
            catch (Exception)
            {

                throw;
            }
            finally  //exception oocur or not, finally block will be executed
            {
                _calObj = null;
            }
        }
    }

}
